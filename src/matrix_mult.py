import numpy as np
import unittest


class TestCuboidVolumn(unittest.TestCase):
    def test_volumn(self):
        self.assertAlmostEqual(cuboid_volumn(2), 8)
        self.assertAlmostEqual(cuboid_volumn(3), 27)
        self.assertAlmostEqual(cuboid_volumn(4), 64)
        self.assertAlmostEqual(cuboid_volumn(5), 125)


def cuboid_volumn(len):
    return len * len * len


if __name__ == "__main__":
    cube_len = [2, 3, 4, 5, 7]
    for item in cube_len:
        print(cuboid_volumn(item))

    a = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    b = np.array([[11, 22, 33], [44, 55, 66], [77, 88, 99]])
    c = np.matmul(a, b)
    print(c)
