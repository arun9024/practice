FROM debian:bullseye

ENV LANG C.UTF-8

RUN apt-get update -qq && apt-get install -qqy --no-install-recommends \
    python3-pip \
    python3-dev \
    build-essential

COPY requirements.txt /srv

WORKDIR /srv

RUN python3 -m pip install --no-cache-dir -r requirements.txt

COPY . /srv

ENV PYTHONPATH $PYTHONPATH:/srv

CMD ["python", "src/matrix_mult.py"]
