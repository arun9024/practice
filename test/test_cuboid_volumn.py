from src.matrix_mult import cuboid_volumn
import unittest


class TestCuboidVolumn (unittest.TestCase):
    def test_voumn(self):
        self.assertAlmostEqual(cuboid_volumn(2), 8)
        self.assertAlmostEqual(cuboid_volumn(3), 27)
        self.assertAlmostEqual(cuboid_volumn(4), 64)
        self.assertAlmostEqual(cuboid_volumn(5), 125)
