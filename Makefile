IMAGE_NAME_SHORT = python_practice

IMAGE_TAG = $(shell git describe --tags --always --dirty)

IMAGE_NAME = arun/$(IMAGE_NAME_SHORT):$(IMAGE_TAG)

build:
	docker build -t $(IMAGE_NAME) -f Dockerfile .
.PHONY: build

lint:
	docker run --rm -t $(IMAGE_NAME) flake8
.PHONY: lint

test:
	docker run $(IMAGE_NAME) pytest
.PHONY: test

clean:
	find . -type f -name *.pyc -delete
	find . -type d -name _pycache_ -delete
.PHONY: clean


